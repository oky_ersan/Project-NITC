<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url("assets/css/open-iconic-bootstrap.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/animate.css") ?>">

    <link rel="stylesheet" href="<?= base_url("assets/css/owl.carousel.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/owl.theme.default.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/magnific-popup.css") ?>">

    <link rel="stylesheet" href="<?= base_url("assets/css/aos.css") ?>">

    <link rel="stylesheet" href="<?= base_url("assets/css/ionicons.min.css") ?>">

    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap-datepicker.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/jquery.timepicker.css") ?>">


    <link rel="stylesheet" href="<?= base_url("assets/css/flaticon.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/icomoon.css") ?>">
    <link rel="stylesheet" href="<?= base_url("assets/css/style.css") ?>">
</head>

<body>

    <?= $this->renderSection('content'); ?>

    <script src="<?= base_url("assets/js/jquery.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery-migrate-3.0.1.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/popper.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery.easing.1.3.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery.waypoints.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery.stellar.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/owl.carousel.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery.magnific-popup.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/aos.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery.animateNumber.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/scrollax.min.js") ?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="<?= base_url("assets/js/google-map.js") ?>"></script>
    <script src="<?= base_url("assets/js/main.js") ?>"></script>

</body>

</html>